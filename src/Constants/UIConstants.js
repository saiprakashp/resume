export const TITLE= 'SAI PRAKASH PULIPAKA';
export const CAREER_HIGHLIGHTS='CAREER HIGHLIGHTS';
export const CONTACT='CONTACT';
export const TECHNICAL_PROFILE='TECHNICAL PROFILE';
export const WEBSITE='WEBSITE';
export const GRADUATION='GRADUATION';
export const INTERN_AND_TRAINING='INTERN AND TRAINING';
export const CURRENT_ORGANIZATION ='CURRENT ORGANIZATION';
export const PROFESSIONAL_EXPERIENCE='PROFESSIONAL EXPERIENCE';

export const  APPLICATION='Application';
export const  PROJECT_DESCRIPTION='Project Description';
export const  RESPONSIBILITIES='Responsibilities';
export const  TECHNOLOGIES='Technologies';


export const COMPANY_1='Ericsson Global India Pvt: ';
export const COMPANY_2='IBM India pvt: ';
export const COMPANY_3='Experis it private Ltd: ';
export const COMPANY_4='Enmovil: ';
