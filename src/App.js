import React from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/js/bootstrap.js';
import User from './User/User';

function App() {

    return (
        <div className="App">
            <User/>
            <footer>
            </footer>
        </div>
    );
}

export default App;
