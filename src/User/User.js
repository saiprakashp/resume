import React from "react";
import * as titles from "../Constants/UIConstants";
import PhoneAndroidTwoToneIcon from "@material-ui/icons/PhoneAndroidTwoTone";
import MailIcon from "@material-ui/icons/Mail";
import LocationOnIcon from "@material-ui/icons/LocationOn";
import logo from "../resources/img/logo.png";
import './User.css';

const User = (props) => {
    return <div>
        <div className="container text-center bg-oc bb-black text-white">
            <div className="row">
                <div className="col-12">
                    <h1>   {titles.TITLE}</h1>
                </div>
            </div>
        </div>

        <div className="card  container border-none">
            <div className="card-body">
                <div className="row">
                    <div className="col-sm-12 col-md-8">
                        <div className="card shadow mb-4">
                            <div className="card-body">
                                <h3 className=" col-or font-weight-bolder sub-title">
                                    {titles.CAREER_HIGHLIGHTS}
                                </h3>
                                <div className="card-text">
                                    <ul>
                                        <li>4years of commendable experience in Java and Web
                                            Development.
                                        </li>
                                        <li> Develop the application using Java, Spring MVC,React.</li>
                                        <li> Good working knowledge on Application development and
                                            maintenance life cycle process.
                                        </li>
                                        <li> Identifying production and non-production application
                                            issues.
                                        </li>
                                        <li> Expose and Consume the web service using
                                            JAX-RS,JAX-WS,Spring.
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div className="card shadow mb-4">
                            <div className="card-body">
                                <h3 className=" col-or font-weight-bolder sub-title">
                                    {titles.TECHNICAL_PROFILE}
                                </h3>
                                <div className="card-text">
                                    <ul> Language: Java
                                        <li>Application/WebServers: WebSphere,Web Logic,Tomcat</li>
                                        <li> FrameworksStruts1.2, Spring (MVC)</li>
                                        <li> Web Technologies: HTML, Bootstrap, CSS, ReactJS, Redux</li>
                                        <li> Databases: Oracle, MariaDB.</li>
                                        <li> IDEs:Eclipse, WebStorm</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div className="card shadow mb-4">
                            <div className="card-body">
                                <h3 className=" col-or font-weight-bolder sub-title">
                                    {titles.INTERN_AND_TRAINING}
                                </h3>
                                <div className="card-text">
                                    <ul>
                                        <li> Enmovil Intern Sep2015 –Feb 2015</li>
                                        <li> ManipalGlobal(Java Training)April 2016-May 2016</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-sm-12 col-md-4">
                        <div className="card shadow mb-4">
                            <div className="card-body">
                                <h3 className=" col-or font-weight-bolder sub-title">
                                    {titles.CONTACT}
                                </h3>
                                <div className="card-text">
                                    <ul className={'ls-dec-no'}>
                                        <li className={'tx-dec-no '}>
                                            <PhoneAndroidTwoToneIcon className={'text-left col-or'}/> <span
                                            className={'text-right'}>9492819199</span>
                                        </li>
                                        <li className={'tx-dec-no mt-1'}><MailIcon
                                            className={'text-left  col-or'}/>
                                            <span
                                                className={'text-right pl-1'}>pulipakasaiprakash@live.com</span>
                                        </li>
                                        <li className={'tx-dec-no mt-1'}><LocationOnIcon
                                            className={'text-left  col-or'}/> <span
                                            className={'text-right'}>Hyderabad, Telangana</span></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div className="card shadow mb-4">
                            <div className="card-body">
                                <h3 className=" col-or font-weight-bolder sub-title">
                                    {titles.WEBSITE}
                                </h3>
                                <div className="card-text">
                                    <ul className={'ls-dec-no'}>
                                        <li>www.pulipakasaiprakash.in</li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div className="card shadow mb-4">
                            <div className="card-body">
                                <h3 className=" col-or font-weight-bolder sub-title">
                                    {titles.GRADUATION}
                                </h3>
                                <div className="card-text">
                                    <ul className={'ls-dec-no'}>
                                        <li>B Tech + M Tech (5 Year Integrated Software Engineering)CSSE
                                            DeptJuly2010 -Aug 2015 Andhra University College of
                                            Engineering Visakhapatnam, Andhra Pradesh
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div className="card shadow mb-4">
                            <div className="card-body">
                                <h3 className=" col-or font-weight-bolder sub-title">
                                    {titles.CURRENT_ORGANIZATION}
                                </h3>
                                <div className="card-text text-center ">
                                    <img src={logo} className='img-thumbnail border-none' width={'30%'}
                                         height={'30%'}/>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div className="container">
            <div className="col-sm-12">
                <div className="card shadow mb-4">
                    <div className="card-body">
                    <h3 className="col-or font-weight-bolder sub-title">

                        {titles.PROFESSIONAL_EXPERIENCE}
                    </h3>
                        <div className='w-100'>

                            <div className="">
                                <div className={'row'}>
                                    <div className='col-sm-6 float-left'><span
                                        className='font-weight-bold'>{titles.COMPANY_1}</span>
                                        Consultant
                                    </div>
                                    <div className='col-sm-6  float-right'>
                                        <span className='font-weight-bold float-right'>April-2019-Present</span>
                                    </div>
                                </div>
                                <div className={'row mt-1'}>
                                    <div className={'col-sm-6'}>
                                                <span
                                                    className='font-weight-bold float-left'>Application 1: CSSOP</span>
                                    </div>
                                </div>
                                <div className={'row mt-1'}>
                                    <div className={'col-sm'}>
                                        <span className='font-weight-bold float-left'>Technologies: </span>
                                        <span className='pl-1'> Java, Struts2.3, Tomcat9, JSP, Servlet, Oracle 12C, Spring MVC, Rest and SOAP Services,Hibernate.</span>
                                    </div>
                                </div>
                                <div className={'row mt-1'}>
                                    <div className={'col-sm'}>
                                        <span className='font-weight-bold float-left'>Project Description: </span>
                                        <span className='pl-1'> CSSOP is a gateway for any VOIP phones to get configured. This could be features or activation of a complete line,trunc, pbx.The configuration or feature are enabled based on multiple system interactions.</span>
                                    </div>
                                </div>
                                <div className={'row mt-1'}>
                                    <div className={'col-sm'}>
                                        <span className='font-weight-bold float-left'>Responsibilities: </span>
                                        <br/>
                                        <ol className=''>
                                            <li>Involved in creating rest-basedservices for migration of project
                                                such as xo customer and smd.
                                            </li>
                                            <li>Involved in migrating ITW to Apigee service migration.</li>
                                            <li>Involved in databasemigrations xo.</li>
                                            <li>Involvedin production support.</li>
                                        </ol>
                                    </div>
                                </div>
                                <div className={'row mt-1'}>
                                    <div className={'col-sm-6'}>
                                                <span
                                                    className='font-weight-bold float-left'>Application 2: Call Forwarding</span>
                                    </div>
                                </div>
                                <div className={'row mt-1'}>
                                    <div className={'col-sm'}>
                                        <span className='font-weight-bold float-left'>Technologies: </span>
                                        <span className='pl-1'> Linux, Apache</span>
                                    </div>
                                </div>
                                <div className={'row mt-1'}>
                                    <div className={'col-sm'}>
                                        <span className='font-weight-bold float-left'>Project Description: </span>
                                        <span className='pl-1'> Call Forwarding is helps user for forwarding calls either landline or mobile</span>
                                    </div>
                                </div>
                                <div className={'row mt-1'}>
                                    <div className={'col-sm'}>
                                        <span className='font-weight-bold float-left'>Responsibilities: </span>
                                        <br/>
                                        <ol className=''>
                                            <li>Involved in H/W migration Apache upgrade and automation process
                                            </li>
                                        </ol>
                                    </div>
                                </div>
                                <div className={'row mt-1'}>
                                    <div className={'col-sm-6'}>
                                                <span
                                                    className='font-weight-bold float-left'>Application 3: PTS (InternalEricsson Productfor all RICO Teams)</span>
                                    </div>
                                </div>
                                <div className={'row mt-1'}>
                                    <div className={'col-sm'}>
                                        <span className='font-weight-bold float-left'>Technologies: </span>
                                        <span className='pl-1'> Java, Struts2.3, Tomcat9, JSP, Servlet, MySQL(MariaDB), Spring, SpringBoot, Rest,Hibernate, React  </span>
                                    </div>
                                </div>
                                <div className={'row mt-1'}>
                                    <div className={'col-sm'}>
                                        <span className='font-weight-bold float-left'>Project Description: </span>
                                        <span className='pl-1'> PTS is a labor clocking and project management system that is deployed in public domain supports new features such as Chatbot, Dynamic report, Custom views and dashboard</span>
                                    </div>
                                </div>
                                <div className={'row mt-1'}>
                                    <div className={'col-sm'}>
                                        <span className='font-weight-bold float-left'>Responsibilities: </span>
                                        <br/>
                                        <ol className=''>
                                            <li>Involved in migrating Complete user interfaces from Jsp to React and
                                                upgrading backend to Spring Boot.
                                            </li>
                                            <li>Creating Jasper Reports, Previously was apache poi based</li>
                                            <li>Creating new modules as per client requires such as WorkFromHome
                                                views ...
                                            </li>
                                        </ol>
                                    </div>
                                </div>

                            </div>


                            <hr/>

                            <div className="mt-2">
                                <div className={'row'}>
                                    <div className='col-sm-6 float-left'><span
                                        className='font-weight-bold'>{titles.COMPANY_2}</span>
                                        Software Engineer
                                    </div>
                                    <div className='col-sm-6  float-right'>
                                        <span className='font-weight-bold float-right'>Aug2016–Mar2019</span>
                                    </div>
                                </div>
                                <div className={'row mt-1'}>
                                    <div className={'col-sm-6'}>
                                                <span
                                                    className='font-weight-bold float-left'>Application 1: Start Stop</span>
                                    </div>
                                </div>
                                <div className={'row mt-1'}>
                                    <div className={'col-sm-6'}>
                                                <span
                                                    className='font-weight-bold float-left'>Technologies Used:</span>
                                        <span className='pl-1'>Core Java, Struts1.2, Web-Sphere 7, WebLogic12, DB2, JSP, Servlet, Oracle 12C, Rest and SOAP Services</span>
                                    </div>
                                </div>
                                <div className={'row mt-1'}>
                                    <div className={'col-sm'}>
                                        <span className='font-weight-bold float-left'>Project Description: </span>
                                        <span className='pl-1'> Start Stop is a gateway for activating or deactivating service obtaining from various channels such as 1) Web Request 2) SMS Request 3) USSD Request 4) Service Request</span>
                                    </div>
                                </div>
                                <div className={'row mt-1'}>
                                    <div className={'col-sm'}>
                                        <span className='font-weight-bold float-left'>Responsibilities: </span>
                                        <br/>
                                        <ol className=''>
                                            <li> Involved in redesigning, performance tuningsomeof existing pages
                                                and created new Services and Service Sorting pages for CSR as the
                                                view was not changed for the past few years.
                                            </li>

                                            <li> Involved in developing new Retry mechanism which was not present so
                                                far inButterfly project.
                                            </li>

                                            <li> Became a new Team member with the client directly and contributed
                                                in developing new 4G, 3G, DND upload and script and
                                                APN integration and Service based Consent Activation within a short
                                                period of time and upgraded existing logics for
                                                increasing performance.
                                            </li>

                                            <li> Involved in migrating the whole system from WebSphere to Web logic
                                                code base, server migrations and new service base migration
                                                along with performance tuning which helped in the growth of the
                                                organization.
                                            </li>

                                            <li> Involved in creating a new Service Integration from various
                                                partners such as Optimus which is the replacement
                                                of NDS and AOC for Consent request.
                                            </li>

                                            <li>Involved in creating new channel integration and collaborated with
                                                different partners for the integration with them.
                                            </li>
                                        </ol>
                                    </div>
                                </div>

                                <div className={'row mt-1'}>
                                    <div className={'col-sm-6'}>
                                                <span
                                                    className='font-weight-bold float-left'>Application 2: AOC</span>
                                    </div>
                                </div>
                                <div className={'row mt-1'}>
                                    <div className={'col-sm-6'}>
                                                <span
                                                    className='font-weight-bold float-left'>Technologies Used:</span>
                                        <span className='pl-1'>Core Java, Struts1.2, Web-Sphere 7, Web-Logic 12, DB2, JSP, Servlet, Oracle 12C, SOAP Service</span>
                                    </div>
                                </div>
                                <div className={'row mt-1'}>
                                    <div className={'col-sm'}>
                                        <span className='font-weight-bold float-left'>Project Description: </span>
                                        <span className='pl-1'> This Project Helps in Taking Consent from a Customer the channel expected is SMS and USSD </span>
                                    </div>
                                </div>
                                <div className={'row mt-1'}>
                                    <div className={'col-sm'}>
                                        <span className='font-weight-bold float-left'>Responsibilities: </span>
                                        <br/>
                                        <ol className=''>
                                            <li> Involved in creating a new channel's for new Channel based consent
                                                and Message Builder.
                                            </li>

                                            <li> Involved in migrating the whole system from WebSphere to
                                                WebLogiccodebase and server migrations along with performance tuning
                                                which helped in the growth of the organization.
                                            </li>

                                        </ol>
                                    </div>
                                </div>


                                <div className={'row mt-1'}>
                                    <div className={'col-sm-6'}>
                                                <span
                                                    className='font-weight-bold float-left'>Application 3: Consent GUI</span>
                                    </div>
                                </div>
                                <div className={'row mt-1'}>
                                    <div className={'col-sm-6'}>
                                                <span
                                                    className='font-weight-bold float-left'>Technologies Used:</span>
                                        <span
                                            className='pl-1'>Core Java, Struts1.2, Web-Logic 12, JSP, Servlet, Oracle 12C</span>
                                    </div>
                                </div>
                                <div className={'row mt-1'}>
                                    <div className={'col-sm'}>
                                        <span className='font-weight-bold float-left'>Project Description: </span>
                                        <span className='pl-1'>This Project Helps in Viewing products for each circle and configuring CP and Sorting products accordingly</span>
                                    </div>
                                </div>
                                <div className={'row mt-1'}>
                                    <div className={'col-sm'}>
                                        <span className='font-weight-bold float-left'>Responsibilities: </span>
                                        <br/>
                                        <ol className=''>
                                            <li>Involved in creating a new view for Operations to sort products as
                                                required.
                                            </li>
                                            <li> Involved in creating CP Product insertion based on File Upload.
                                            </li>
                                            <li>Involved in migrating the whole system from WebSphere to
                                                WebLogiccodebase and server setup, Hardware setup, migrations along
                                                with performance tuning which helped in the growth of the
                                                organization.
                                            </li>

                                        </ol>
                                    </div>
                                </div>

                                <div className={'row mt-1'}>
                                    <div className={'col-sm'}>
                                        ( <span
                                        className='font-weight-bold float-left'>Experis it private Ltd </span>(IBMcontractor)
                                        August2016-April 2018)
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
};
export default User;
